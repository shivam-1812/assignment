package com.instawork.search;

import com.instawork.utils.Helper;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GoogleSearchPage extends Helper implements GoogleSearchPageLocator {

    public GoogleSearchPage(RemoteWebDriver driver) {
        super(driver);
    }

    public void enterTextInSearch(String input){
        find(SEARCH_FIELD).sendKeys(input);
    }

    public void clickOnSearch(){
        find(SEARCH_BUTTON).click();
    }
}
